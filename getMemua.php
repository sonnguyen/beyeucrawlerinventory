﻿<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit','512M');
ini_set('max_execution_time', 0); 
 
header('Content-Type: text/html; charset=utf-8');

include("lib/simple_html_dom.php"); 
//print_r($_SESSION);
$url = 'http://memua.vn';

$filedata = "datame.txt";
$filecheck= "positionme.txt";
$arrchk = readCheckArray($filecheck);
if(isset($arrchk) && !empty($arrchk)){
     foreach($arrchk as $key=>$value){
        $arrdata = readDataArray($filedata);
        $subarrchk = $arrchk[$key];
         if(!is_array($subarrchk) || $subarrchk == 0){
            //loop each item in check and put data to existing array.
            $subarr = $arrdata[$key];
            $number = 0;
            getCount($url.$key, 1, $number, $url);
            if(!empty($subarr)){
                $clsinput = 'dropdown dropdown-vertical';
                $subdata = getData($clsinput, $url.$key);
                
                if(isset($subdata) && !empty($subdata)){
                    $arrdata[$key] = array('title'=>$arrdata[$key]. '('.$number.') ', 'subcat'=>$subdata);
                    $arrchk[$key] = $subdata;
                    writeDataArray($filedata, $arrdata);
                    writeDataArray($filecheck, $arrchk);
                }else{
                    if(strpos($arrdata[$key],'Array')===false){
                        $arrdata[$key] = array('title'=>$arrdata[$key].' ('.$number.')');
                        writeDataArray($filedata, $arrdata);
                    }
                }
            }else{
                $arrchk[$key] = 1;
            }
         }else{
             foreach($subarrchk as $subkey=>$subvalue){
                 $subsubarrchk = $arrchk[$key][$subkey];
                 if(!is_array($subsubarrchk)){
                     $subsubarr = $arrdata[$key]['subcat'][$subkey];
                    $number = 0;
                    getCount($url.$subkey, 1, $number, $url);
                     if(!empty($subsubarr)){
                            $clsinput = 'b-border dir cm-active';
                            $subdata = getSubData($clsinput, $url.$subkey);
                            if(isset($subdata) && !empty($subdata)){
                                $arrdata[$key]['subcat'][$subkey] = array('title'=>$arrdata[$key]['subcat'][$subkey]. ' ('.$number.')', 'subcat'=>$subdata);
                                $arrchk[$key][$subkey] = $subdata;
                                writeDataArray($filedata, $arrdata);
                                writeDataArray($filecheck, $arrchk);
                            }else{
                                if(strpos($arrdata[$key]['subcat'][$subkey],'Array')===false){
                                    if(!strpos($arrdata[$key]['subcat'][$subkey]['title'], '(')){
                                        $arrdata[$key]['subcat'][$subkey] = array('title'=>$arrdata[$key]['subcat'][$subkey].' ('.$number.')');
                                        writeDataArray($filedata, $arrdata);
                                    }
                                }
                            }
                     }else{
                         $arrchk[$key][$subkey] = 1;
                     }
                 }else{
                     foreach($subsubarrchk as $subsubkey=>$subsubvalue){
                            $number = 0;
                            getCount($url.$subsubkey, 1, $number, $url);
                            if(strpos($arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey],'Array')===false){
                                if(!strpos($arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey]['title'], '(')){
                                    $arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey] = array('title'=>$arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey].' ('.$number.')');
                                    writeDataArray($filedata, $arrdata);
                                }
                            }
                     }
                 }
             }
         }
    }
}else{
    $clsinput = 'dropdown-multicolumns';
    $data = getData($clsinput, $url);
    writeDataArray($filedata, $data);
    writeCheckArray($filecheck, $data);
}

function getData($clsinput, $url){
    
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $query = "//*[contains(concat(' ', normalize-space(@class), ' '), ' $clsinput ')]/li/a";
    
    foreach($x->query($query) as $key=>$node){
        $path = $node->getAttribute("href");
        $data[$path] = trim(preg_replace('/\s+/',' ', $node->nodeValue));
    }
    return $data;
}


function getSubData($clsinput, $url){
    
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $query = "//*[contains(concat(' ', normalize-space(@class), ' '), ' $clsinput ')]/ul/li/a";
    
    foreach($x->query($query) as $key=>$node){
        $path = $node->getAttribute("href");
        $data[$path] = trim(preg_replace('/\s+/',' ', $node->nodeValue));
    }
    return $data;
}

function getMenuData($clsinput, $url){
    
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $query = "//*[contains(concat(' ', normalize-space(@class), ' '), ' $clsinput ')]/a";
    $rsl = $x->query($query);
    foreach($rsl as $key=>$node){
        $path = $node->getAttribute("href");
        $data['path'] = $path;
        if($key == 2)
        break;
    }
    return $data;
}
function getCount($url, $level, &$return, $original){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    switch ($level){
        case 1:
            $rsl = $x->query("(//*[contains(concat(' ', normalize-space(@class), ' '), ' cm-history next ')])[last()]");
            if($rsl->length==0){
                $subrsl = $x->query("(//*[contains(concat(' ', normalize-space(@rev), ' '), ' pagination_contents ')])[last()]");
                $totalpage = $subrsl->item(0)->nodeValue;
                if($subrsl->length == 0){
                    $inputdom1 = file_get_contents($url);
                    $dom1 = new DOMDocument(); 
                    @$dom1->loadHTML($inputdom1); 
                    $x1 = new DOMXPath($dom1);  

                    $rslitem = $x1->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' center-block ')]");
                    $subitem = $rslitem->length;
                    $return = $subitem;
                }elseif(is_numeric($totalpage)){
                    $count1 = ($totalpage-1)*32;

                    $inputdom1 = file_get_contents($original.$subrsl->item(0)->getAttribute('href'));
                    $dom1 = new DOMDocument(); 
                    @$dom1->loadHTML($inputdom1); 
                    $x1 = new DOMXPath($dom1);  

                    $rslitem = $x1->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' center-block ')]");
                    $subitem = $rslitem->length;
                    $return = $subitem + $count1;
                }else{
                    getCount($url, 2, $return, $original);
                }
            }else{
                getCount($original.$rsl->item(0)->getAttribute('href'), 1, $return, $original);
            }
            break;
        case 2:
            $subrsl = $x->query("(//*[contains(concat(' ', normalize-space(@rev), ' '), ' pagination_contents ')])[last()-1]");
            $totalpage = $subrsl->item(0)->nodeValue;
            if($subrsl->length == 0){
                $inputdom1 = file_get_contents($url);
                $dom1 = new DOMDocument(); 
                @$dom1->loadHTML($inputdom1); 
                $x1 = new DOMXPath($dom1);  

                $rslitem = $x1->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' center-block ')]");
                $subitem = $rslitem->length;
                $return = $subitem;
            }elseif(is_numeric($totalpage)){
                $count2 = ($totalpage-1)*32;
                $inputdom1 = file_get_contents($original.$subrsl->item(0)->getAttribute('href'));
                $dom1 = new DOMDocument(); 
                @$dom1->loadHTML($inputdom1); 
                $x1 = new DOMXPath($dom1);  

                $rslitem = $x1->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' center-block ')]");
                $subitem = $rslitem->length;
                $return = $subitem + $count2;
    //            return $count;
            }else{
                getCount($original.$subrsl->item(0)->getAttribute('href'), 3, $return, $original);
            }
            break;
        case 3:
            $subrsl = $x->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' product-cell ')]");
            $return = $subrsl->length;
            break;
        default:
            break;
    }
}

function getShape($clsinput, $url){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $rsl = $x->query("//*[contains(@shape, '$clsinput')]");
    foreach($rsl as $key=>$node){
        $path = $node->getAttribute("href");
        $data['path'] = $path;
        if($key == 2)
        break;
//        $data[$path] = trim(preg_replace('/\s+/',' ', $node->getAttribute("alt")));
    }
    return $data;
}

function readDataArray($file){
    $getData = file_get_contents($file);
    $rtarr = unserialize($getData);
    return $rtarr;
}
function writeDataArray($file, $arrInput){
    $myString = serialize($arrInput);
    file_put_contents($file, $myString);
}
function readCheckArray($file){
    $getData = file_get_contents($file);
    $rtarr = unserialize($getData);
    return $rtarr;
}
function writeCheckArray($file, $arrInput){
    foreach($arrInput as $key=>$subarr){
        $arrInput[$key] = 0;
    }
    $myString = serialize($arrInput);
    file_put_contents($file, $myString);
}
//$html = new DOMDocument();
//@$html->loadHtmlFile('http://www.memua.com');
//$xpath = new DOMXPath( $html );
//$nodelist = $xpath->query( "//div[@class='wrap-dropdown-multicolumns']/ul/li/a/@href" );
//foreach ($nodelist as $n){
//    echo $n->nodeValue."\n<br>";
//}
?>