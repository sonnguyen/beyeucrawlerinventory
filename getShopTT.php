﻿<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
 
//header('Content-Type: text/html; charset=utf-8');

include("lib/simple_html_dom.php"); 
//print_r($_SESSION);
$url = 'http://shoptretho.com.vn';
//print '<pre>';
//print_r(getItemList($url));
//print '</pre>';
//die;

$filedata = "datastt.txt";
$filecheck= "positionstt.txt";
$arrchk = readCheckArray($filecheck);

if(isset($arrchk) && !empty($arrchk)){
     foreach($arrchk as $key=>$value){
        $arrdata = readDataArray($filedata);
        $subarrchk = $arrchk[$key];
         if(!is_array($subarrchk) || $subarrchk == 0){
            //loop each item in check and put data to existing array.
            $subarr = $arrdata[$key];
            if(!empty($subarr)){
                $clsinput = 'item-catelv3';
                $subdata = getData($clsinput, $url.$key);
                $countdata = getCount($url.$key);
                if(isset($subdata) && !empty($subdata)){
                    $arrdata[$key] = array('title'=>$arrdata[$key].' ('.$countdata.')' ,'subcat'=>$subdata);
                    $arrchk[$key] = $subdata;
                    writeDataArray($filedata, $arrdata);
                    writeDataArray($filecheck, $arrchk);
                }else{
                    if(strpos($arrdata[$key],'Array')===false){
                        $arrdata[$key] = array('title'=>$arrdata[$key].' ('.$countdata.')');
                        writeDataArray($filedata, $arrdata);
                    }
                }
            }else{
                $arrchk[$key] = 1;
            }
         }else{
             foreach($subarrchk as $subkey=>$subvalue){
                 $subsubarrchk = $arrchk[$key][$subkey];
                 if(!is_array($subsubarrchk)){
                     $subsubarr = $arrdata[$key]['subcat'][$subkey];
                     if(!empty($subsubarr)){
                            $clsinput = 'bor-catelv3';
                            $subcountdata = getData($clsinput, $url.$subkey);
                            $countdata = getCount($url.$subkey);
                
                            if(isset($subcountdata) && !empty($subcountdata)){
                                $arrdata[$key]['subcat'][$subkey] = array('title'=>$arrdata[$key]['subcat'][$subkey].' ('.$countdata.')', 'subcat'=>$subcountdata);
                                $arrchk[$key][$subkey] = $subcountdata;
                                writeDataArray($filedata, $arrdata);
                                writeDataArray($filecheck, $arrchk);
                            }else{
                                if(strpos($arrdata[$key]['subcat'][$subkey],'Array')===false){
                                    $arrdata[$key]['subcat'][$subkey] = array('title'=>$arrdata[$key]['subcat'][$subkey].' ('.$countdata.')');
                                    writeDataArray($filedata, $arrdata);
                                }
                            }
                     }else{
                         $arrchk[$key][$subkey] = 1;
                     }
                 }else{
                    foreach($subsubarrchk as $subsubkey=>$subsubvalue){
                        $subsubsubarrchk = $arrchk[$key][$subkey][$subsubkey];
                        if(!is_array($subsubsubarrchk)){
                            $subsubsubarr = $arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey];
                            if(!empty($subsubsubarr)){
                                   $clsinput = 'bor-catelv3';
                                   $subcountdata = getData($clsinput, $url.$subsubkey);
                                    $countdata = getCount($url.$subsubkey);
                                    
                                   if(isset($subdata) && !empty($subdata)){
                                       $arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey] = array('title'=>$arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey].' ('.$countdata.')', 'subcat'=>$subcountdata);
                                       $arrchk[$key][$subkey][$subsubkey] = $subcountdata;
                                       writeDataArray($filedata, $arrdata);
                                       writeDataArray($filecheck, $arrchk);
                                   }else{
                                       if(strpos($arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey],'Array')===false){
                                            $arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey] = array('title'=>$arrdata[$key]['subcat'][$subkey]['subcat'][$subsubkey].' ('.$countdata.')');
                                            writeDataArray($filedata, $arrdata);
                                       }
                                   }
                            }else{
                                $arrchk[$key][$subkey][$subsubkey] = 1;
                            }
                        }
                    }
                }
             }
         }
    }
}else{
    $clsinput = 'nav_browse_ul';
    $data = getMainCat($clsinput, $url);
    writeDataArray($filedata, $data);
    writeCheckArray($filecheck, $data);
}
function getItems($url){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $title = '';
    $price = '';
    $query = "//*[contains(concat(' ', normalize-space(@class), ' '), ' block-normal ')]/div[1]";
    
    foreach($x->query($query) as $key=>$node){
        $title[] = trim(preg_replace('/\s+/',' ', $node->nextSibling->nextSibling->nodeValue));
    }
    return $title;
}

function getItemList($url){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $total = 0;
    $rsl = $x->query("//*[contains(@class, 'fl result-count')]/strong");
    if($rsl->length > 0){
        $total = $rsl->item(0)->nodeValue;
    }
    
    $qrcntprod = "//*[contains(concat(' ', normalize-space(@class), ' '), ' block-normal col-175 ')]/div[1]";
    $rslcntcprod = $x->query($qrcntprod);
    $cntprod = $rslcntcprod->length;
    $data = array();
    if($total > $cntprod){
        $totalpage = ceil($total/$cntprod);
        for($i = 1; $i<=$totalpage; $i++){
            $temp = getItems($url.'?page='.$i);
            $data = array_merge($temp,$data);
        }
    }else{
        $temp = getItems($url);
        $data = array_merge($temp,$data);
    }
    return $data;
}

function getMainCat($clsinput, $url){
    
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $query = "//*[contains(concat(' ', normalize-space(@class), ' '), ' $clsinput ')]/li/div/a";
    
    foreach($x->query($query) as $key=>$node){
        $path = $node->getAttribute("href");
        $data[$path] = trim(preg_replace('/\s+/',' ', $node->nodeValue));
    }
    return $data;
}

function getData($clsinput, $url){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $rsl = $x->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $clsinput ')]/a");
    foreach($rsl as $key=>$node){
        $path = $node->getAttribute("href");
        $data[$path] = trim(preg_replace('/\s+/',' ', $node->nodeValue));
    }
//    $index = $rsl->length;
//    unset($data[$index-1]);
    return $data;
}

function getDataLevel1($clsinput, $url){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = array();
    $rsl = $x->query("//*[contains(@class, '$clsinput')]/following-sibling::ul/li/a");
    if($rsl->length > 0){
        foreach($rsl as $key=>$node){
            $path = $node->getAttribute("href");
            $data[$path] = trim(preg_replace('/\s+/',' ', $node->nodeValue));
        }
    }
    return $data;
}

function getCount($url){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = 0;
    $rsl = $x->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' bor_catelv2 ')]/h2/font[2]");
    foreach($rsl as $key=>$node){
        $data = trim(preg_replace('/\s+/',' ', $node->nodeValue));
    }
    return $data;
}

function getCountBook($url){
    $inputdom = file_get_contents($url);
    
    $dom = new DOMDocument(); 
    @$dom->loadHTML($inputdom); 
    $x = new DOMXPath($dom);  
    
    $data = 0;
    $rsl = $x->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' col-175 hover-block ')]/div[1]");
//    if($rsl->length > 0){
//        $data = $rsl->item(0)->nodeValue;
//    }
    return $rsl->length;
}

function readDataArray($file){
    $getData = file_get_contents($file);
    $rtarr = unserialize($getData);
    return $rtarr;
}
function writeDataArray($file, $arrInput){
    $myString = serialize($arrInput);
    file_put_contents($file, $myString);
}
function readCheckArray($file){
    $getData = file_get_contents($file);
    $rtarr = unserialize($getData);
    return $rtarr;
}
function writeCheckArray($file, $arrInput){
    foreach($arrInput as $key=>$subarr){
        $arrInput[$key] = 0;
    }
    $myString = serialize($arrInput);
    file_put_contents($file, $myString);
}
//$html = new DOMDocument();
//@$html->loadHtmlFile('http://www.memua.com');
//$xpath = new DOMXPath( $html );
//$nodelist = $xpath->query( "//div[@class='wrap-dropdown-multicolumns']/ul/li/a/@href" );
//foreach ($nodelist as $n){
//    echo $n->nodeValue."\n<br>";
//}
?>