<?

// this class implements a thread safe queue.
class load_queue
{
   // slightly slow way to push all urls into the database.
   /// if they are already there, it overwrites them.
   // returns number of urls loaded into db.
   public function pushAll($urls,$type,$forceLoad=false)
   {
      $type = db::quote($type);

      $count = 0;
      try
      {
         db::BEGIN();
         foreach ($urls as $url)
         {
            $url = db::quote($url);
            if ($forceLoad)
            {
               $this->expireUrl($url,$type);
            }
            $count+= db::query("INSERT IGNORE INTO load_queue (url,processing,type) VALUES('$url',0,'$type')");
         }
         db::COMMIT();
      }
      catch (Exception $e)
      {
         db::ROLLBACK();
         log::error("Exception ".$e->getMessage());
      }      


      return $count;
   }

   // push a single url to the queue
   
   public function push($url,$type,$forceLoad=false)
   {

      $this->pushAll(array($url), $type,$forceLoad);
   }

   # grabs a url off the load queue of type $type
   public function pop($type)
   {
      $type = db::quote($type);
      log::debug_verbose("Pulling New URL off load_queue");
      try
      {
         db::BEGIN();
         $sql = "SELECT id,url from load_queue where processing = 0 and type = '$type' order by timeactionmade desc LIMIT 1";
         
         log::debug_verbose("running $sql");

         $r = db::query($sql);    
         $url = "";

         if ($r)
         {
            db::query("UPDATE load_queue set processing = 1 WHERE id = {$r['id']}");
            $url  = $r['url'];
         }
        
         db::COMMIT();
      }
      catch (Exception $e)
      {
         db::ROLLBACK();
         log::error("Exception ".$e->getMessage());
      }      

      log::debug("Got New URL from load_queue: '$url'");
      return $url;
   }

   static function expireUrl($url,$type)
   {
      $sql = "UPDATE load_queue set processing=0 WHERE type = '$type' AND url = '$url'";
      log::debug_verbose($sql);
      db::query($sql);

      $sql = "UPDATE raw_data set parsed=0 WHERE type = '$type' AND url = '$url'";
      log::debug_verbose($sql);
      db::query($sql);

   }
   // expires urls that are older than days days.
   function expireQueue($days,$type,$url)
   {
      $days = db::quote($days);
      $type = db::quote($type);
      $sql = "DELETE l,r FROM load_queue l LEFT JOIN raw_data r ON (l.url=r.url and l.type=r.type) where DATEDIFF(NOW(), l.timeactionmade) > $days and l.type = '$type'";
      log::debug_verbose($sql);
      db::query($sql);
   }

   function numToProcess($type)
   {
      $sql = "SELECT count(*) FROM load_queue WHERE type = '$type' and processing = 0";
      return db::oneCell($sql);

   }
}